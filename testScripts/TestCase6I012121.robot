*** Settings ***
Suite Setup    Open Browser    https://www.katalon.com/    firefox
Suite Teardown    Close Browser
Resource    seleniumLibrary.robot

*** Variables ***
${undefined}    https://www.katalon.com/

*** Test Cases ***
Test Case
    open    http://localhost:8000/index.html
    setText    id=c1    0
    setText    id=c2    12
    setText    id=c3    12
    click    xpath=(.//*[normalize-space(text()) and normalize-space(.)='Compute Information - I(c1,c2,c3) = Result'])[1]/following::button[1]
    assertText    id=result    I(0, 12, 12) = 1
    