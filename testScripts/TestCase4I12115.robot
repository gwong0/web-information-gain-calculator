*** Settings ***
Suite Setup    Open Browser    https://www.katalon.com/    firefox
Suite Teardown    Close Browser
Resource    seleniumLibrary.robot

*** Variables ***
${undefined}    https://www.katalon.com/

*** Test Cases ***
Test Case
    open    http://localhost:8000/index.html
    setText    id=c1    1
    setText    id=c2    2
    setText    id=c3    1
    click    xpath=(.//*[normalize-space(text()) and normalize-space(.)='Compute Information - I(c1,c2,c3) = Result'])[1]/following::button[1]
    assertText    id=result    I(1, 2, 1) = 1.5
    