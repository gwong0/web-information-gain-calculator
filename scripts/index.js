function positiveWholeIntegersOnly(event) {
	return (
    	event.charCode == 8 || 
        event.charCode == 0 || 
        event.charCode == 13
	) ? 
    null : 
	event.charCode >= 48 && event.charCode <= 57;
}

function writeFormula(c1,c2,c3) {
	let output = "";
    
    output = output.concat("I(");
    
    if ((c1==0 || c1) && !isNaN(c1)) {
	    output = output.concat(c1);
    }

	if ((c2==0 || c2) && !isNaN(c2)) {
		output = output.concat(", ").concat(c2);
    }

	if ((c3==0 || c3) && !isNaN(c3)) {
	    output = output.concat(", ").concat(c3);
    }
    
	output = output.concat(") = ");
    
	return output;
}

function computeI(u, l, d) {

	let uldSum = u+l+d;

	let uDivByUldSum = u / uldSum;
	let lDivByUldSum = l / uldSum;
	let dDivByUldSum = d / uldSum;

	let c1 = -(uDivByUldSum * Math.log2(uDivByUldSum));
	let c2 = -(lDivByUldSum * Math.log2(lDivByUldSum));
	let c3 = -(dDivByUldSum * Math.log2(dDivByUldSum));

	let normC1 = 0;
	if (!isNaN(c1)) {
    	normC1 = c1;
	}

	let normC2 = 0;
	if (!isNaN(c2)) {
    	normC2 = c2;
	}

	let normC3 = 0;
	if (!isNaN(c3)) {
    	normC3 = c3;
	}
	
	let out = normC1 + normC2 + normC3;
	
    return out;
}

function callComputeI() {
	let c1 = parseInt(document.getElementById("c1").value);
	let c2 = parseInt(document.getElementById("c2").value);
	let c3 = parseInt(document.getElementById("c3").value);
    
    let output = '';
    
    if (isNaN(c1) || isNaN(c2) || isNaN(c3)) {
    	output = '0';
    }
    
    output = writeFormula(c1, c2, c3).concat(computeI(c1, c2, c3));
    
	document.getElementById('result').innerHTML = output;
}
